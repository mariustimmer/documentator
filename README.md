Documentator
============


Requirements
------------
To execute this documentator you need to install `php` on your system. Additionally you need the packages `php-json` and `php-gettext`. I never tested to run it under Windows but it is written and tested on a GNU/Linux Ubuntu

If your system fullfills this requirements Documentator is ready to work.


Usage
-----
Use Documentator on a terminal by just executing the [`bin/documentator`](bin/documentator) command. If you run in without any arguments the help will be printed to show you the available commands.


License
-------
This software is licensed under the GNU AFFERO GENERAL PUBLIC LICENSE version 3. For more details have a look into the [LICENSE](LICENSE.md) file


Author
------
This application was written because I needed something like it and couldn't find it on the net. If you want to contribute to this project feel free to contact me. But until now everything is written by me: [Marius Timmer](mailto:marius.timmer@web.de)
