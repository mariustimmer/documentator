<?php

class BaseAutoloader {

	public static function autoload(string $classpath) {
		$filename = __DIR__ . DIRECTORY_SEPARATOR . str_replace("\\", '/', $classpath) .'.php';
		if (file_exists($filename)) {
			require_once($filename);
			return true;
		}
		return false;
	}

}

define('ROOT_DIRECTORY', __DIR__ . DIRECTORY_SEPARATOR .'..');
spl_autoload_register(
	[
		BaseAutoloader::class,
		'autoload'
	]
);

