<?php

namespace org\documentator;

use \org\documentator\CommandHandler AS CommandHandler;
use \org\documentator\Documentator AS Documentator;
use \org\documentator\AddCommand AS AddCommand;
use \org\documentator\ListTagCommand AS ListTagCommand;
use \org\documentator\IntegrityCommand AS IntegrityCommand;

class HelpCommand extends CommandHandler {

    public function __construct(array $arguments) {
        parent::__construct(
            $arguments,
            gettext("Prints out this help"),
            '',
            0
        );
    }

	public function run() {
		$data = [
			Documentator::COMMAND_HELP      => $this,
			Documentator::COMMAND_ADD       => new AddCommand([0,0,0]),
            Documentator::COMMAND_LISTTAGS  => new ListTagCommand([0,0]),
            Documentator::COMMAND_SEARCH    => new SearchCommand([0,0,0]),
            Documentator::COMMAND_INTEGRITY => new IntegrityCommand([0,0])
		];
		fprintf(
			STDOUT,
			"Documentator [v.%s] - %s\n",
			Documentator::VERSION,
			gettext("Easy to use document manager")
		);
		foreach ($data AS $command => $commandhandler) {
			fprintf(
				STDOUT,
				"  %12s %-20s %s\n",
				$command,
				$commandhandler->getArgumentDescription(),
				$commandhandler->getDescription()
			);
		}
	}

}
