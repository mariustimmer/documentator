<?php

namespace org\documentator;

use \Exception AS Exception;

class DataStorage {

    const DATA_FILE = ROOT_DIRECTORY . DIRECTORY_SEPARATOR .'data.json';
    const KEY_USER      = 'user';
    const KEY_CREATED   = 'created';
    const KEY_DOCUMENTS = 'documents';
    const KEY_FILENAME  = 'filename';
    const KEY_TAGS      = 'tags';
    const KEY_UPDATED   = 'updated';
    const KEY_MATCHES   = 'matches';
    const KEY_CHECKSUM  = 'checksum';

    /* @var DataStorage $instance */
    private static $instance;

    /* @var object $data */
    private $data;

    private function __construct() {
        $this->initDatafile();
        $this->load();
    }

    public static function getInstance(): DataStorage {
        if (!self::$instance) {
            self::$instance = new DataStorage();
        }
        return self::$instance;
    }

    public function getOwner(): string {
        return $this->data[self::KEY_USER];
    }

    public function getCreated(): string {
        return $this->data[self::KEY_CREATED];
    }

    public function containsDocument(string $name): bool {
        return (isset($this->data[self::KEY_DOCUMENTS][$name]));
    }

    public function getAllTags(): array {
        $tags = [];
        foreach ($this->data[self::KEY_DOCUMENTS] AS $name => $attributes) {
            foreach ($attributes[self::KEY_TAGS] AS $tag) {
                if (!isset($tags[$tag])) {
                    $tags[$tag] = 0;
                }
                $tags[$tag]++;
            }
        }
        return $tags;
    }

    public function search(array $tags): array {
        $results = [];
        foreach ($this->data[self::KEY_DOCUMENTS] AS $name => $attributes) {
            foreach ($attributes[self::KEY_TAGS] AS $doc_tag) {
                foreach ($tags AS $search_tag) {
                    if (strpos(strtolower($doc_tag), strtolower($search_tag)) !== false) {
                        if (!isset($results[$name])) {
                            $results[$name] = $attributes;
                        }
                        if (!isset($results[$name][self::KEY_MATCHES])) {
                            $results[$name][self::KEY_MATCHES] = [];
                        }
                        array_push($results[$name][self::KEY_MATCHES], $search_tag);
                    }
                }
            }
        }
        return $results;
    }

    /**
     * Returns all known documents in this data storage.
     * @return Array with all document data
     */
    public function getAllDocuments(): array {
        return $this->data[self::KEY_DOCUMENTS];
    }

    public function getDocument(string $name) {
        if (!$this->containsDocument($name)) {
            return null;
        }
        return $this->data[self::KEY_DOCUMENTS][$name];
    }

    /**
     * Puts a document to the data storage even if it already exists.
     * @param $filename Path to the file
     * @param $document Document data
     */
    public function putDocument(string $filename, array $document) {
        $this->data[self::KEY_DOCUMENTS][$filename] = $document;
    }

    /**
     * Adds a new document to the data storage.
     * @param $filename Path to the file
     * @param $tags List of Tags to use for this file
     * @return bool True
     */
	public function addDocument(string $filename, array $tags): bool {
        $update = $this->containsDocument($filename);
        if (empty($this->data[self::KEY_DOCUMENTS])) {
            /**
             * If this is the first document the attribute has to be
             * initialized as two dimensional array first.
             */
            $this->data[self::KEY_DOCUMENTS] = [];
        }
        $this->data[self::KEY_DOCUMENTS][$filename] = [
            self::KEY_FILENAME => $filename,
            self::KEY_TAGS     => array_unique($tags),
            self::KEY_UPDATED  => time(),
            self::KEY_CHECKSUM => md5_file($filename)
        ];
        $this->save();
        return true;
	}

    /**
     * Loads and parses the JSON data from the file.
     */
	private function load() {
		$this->data = json_decode(
			file_get_contents(
				self::DATA_FILE
			),
            true
		);
        if ($this->data === null) {
            throw new Exception(
                sprintf(
                    "Could not parse JSON from \"%s\"",
                    self::DATA_FILE
                )
            );
        }
	}

    /**
     * Writes the current data to the data file.
     */
    public function save() {
        $json = json_encode($this->data, JSON_PRETTY_PRINT);
        if ($json === null) {
            throw new Exception(gettext("Could not convert data to JSON"));
        }
        $written_bytes = file_put_contents(
            self::DATA_FILE,
            $json
        );
        if ($written_bytes === false) {
            throw new Exception(
                sprintf(
                    gettext("Could not write data file \"%s\""),
                    self::DATA_FILE
                )
            );
        }
    }

	/**
	 * Makes sure the data file does exists.
	 */
	private function initDatafile() {
		if (!file_exists(self::DATA_FILE)) {
            /**
             * There is no data file so we create a new one with default data.
             */
			$this->data = [
				self::KEY_CREATED   => time(),
				self::KEY_USER      => get_current_user(),
				self::KEY_DOCUMENTS => []
			];
			$this->save();
		}
	}

}
