<?php

namespace org\documentator;

use \Exception AS Exception;
use \org\documentator\AddCommand AS AddCommand;
use \org\documentator\ListTagCommand AS ListTagCommand;
use \org\documentator\HelpCommand AS HelpCommand;
use \org\documentator\IntegrityCommand AS IntegrityCommand;
use \org\documentator\DataStorage AS DataStorage;

class Documentator {

	const VERSION = '0.1';
	const COMMAND_HELP      = 'help';
	const COMMAND_ADD       = 'add';
	const COMMAND_LISTTAGS  = 'taglist';
	const COMMAND_SEARCH    = 'search';
	const COMMAND_INTEGRITY = 'integrity';

	public static function run(array $arguments) {
		if (count($arguments) == 1) {
			/**
			 * If no argument was supplied we fall back to the
			 * help site so the user will be informed about all
			 * available commands.
			 */
			array_push($arguments, self::COMMAND_HELP);
		}
		$command = $arguments[1];
		$commandhandler = null;
		try {
            /**
             * Creating the DataStorage instance will ensure the data file does
             * exists. If it does not it will be created. If that fails the
             * exception will be thrown and cought.
             */
			DataStorage::getInstance();
			switch ($command) {
				case self::COMMAND_ADD:
					$commandhandler = new AddCommand($arguments);
					break;
				case self::COMMAND_LISTTAGS:
					$commandhandler = new ListTagCommand($arguments);
					break;
				case self::COMMAND_SEARCH:
					$commandhandler = new SearchCommand($arguments);
					break;
				case self::COMMAND_INTEGRITY:
					$commandhandler = new IntegrityCommand($arguments);
					break;
				case self::COMMAND_HELP:
				default:
					$commandhandler = new HelpCommand($arguments);
			}
			if ($commandhandler !== null) {
				$commandhandler->run();
			}
		} catch (Exception $exception) {
			fprintf(
				STDERR,
				"%s: %s\n",
				gettext("Fatal error"),
				$exception->getMessage()
			);
			exit(1);
		}
	}

}
