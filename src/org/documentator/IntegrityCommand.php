<?php

namespace org\documentator;

use \org\documentator\CommandHandler AS CommandHandler;
use \org\documentator\DataStorage AS DataStorage;

class IntegrityCommand extends CommandHandler {

    const COLOR_RED    = "\e[0;31m";
    const COLOR_GREEN  = "\e[0;32m";
    const COLOR_YELLOW = "\e[1;33m";
    const COLOR_RESET  = "\e[0m";

    public function __construct(array $arguments) {
        parent::__construct(
            $arguments,
            gettext("Runs an integrity check against all known files"),
            '',
            0
        );
    }

	public function run() {
        foreach (DataStorage::getInstance()->getAllDocuments() AS $file_path => $document) {
            $notice = null;
            $exists = file_exists($file_path);
            $file_verified = false;
            if ($exists) {
                $file_checksum = md5_file($file_path, false);
                if (!isset($document[DataStorage::KEY_CHECKSUM])) {
                    /**
                     * The file exists but no checksum is known. In this case
                     * the checksum will be generated and stored to the data
                     * storage.
                     * Additionally the user should be informed about this change.
                     */
                    $document[DataStorage::KEY_CHECKSUM] = $file_checksum;
                    DataStorage::getInstance()->putDocument($file_path, $document);
                    $notice = gettext("New checksum added");
                }
                $file_verified = ($file_checksum == $document[DataStorage::KEY_CHECKSUM]);
                if (!$file_verified) {
                    $notice = sprintf(
                        gettext("Old checksum is '%s' but the files checksum is '%s'"),
                        $document[DataStorage::KEY_CHECKSUM],
                        $file_checksum
                    );
                }
            }
            fprintf(
                STDOUT,
                " [%s%s]\t\"%s\"%s\n",
                ($exists) ? ($file_verified) ? self::COLOR_GREEN . gettext('   OK   ') : self::COLOR_RED . gettext('MODIFIED') : self::COLOR_RED . gettext('  LOST  '),
                self::COLOR_RESET,
                $file_path,
                is_null($notice) ? '' : ' ('. self::COLOR_YELLOW . $notice . self::COLOR_RESET .')'
            );
        }
        DataStorage::getInstance()->save();
	}

}
