<?php

namespace org\documentator;

use \org\documentator\DataStorage AS DataStorage;

class ListTagCommand extends CommandHandler {

    public function __construct(array $arguments) {
        parent::__construct(
            $arguments,
            gettext("Lists all tags used for the documents"),
            '',
            0
        );
    }

    public function run() {
        $tags = DataStorage::getInstance()->getAllTags();
        foreach ($tags AS $tag => $amount) {
            fprintf(
                STDOUT,
                " % 4d  %s\n",
                $amount,
                $tag
            );
        }
        fprintf(
            STDOUT,
            gettext("%d tags") ."\n",
            count($tags)
        );
    }

}
