<?php

namespace org\documentator;

use \org\documentator\DataStorage AS DataStorage;

class SearchCommand extends CommandHandler {

    public function __construct(array $arguments) {
        parent::__construct(
            $arguments,
            gettext("Searches for a document based on the tags"),
            gettext("<tags>"),
            1
        );
    }

    private function sortResults(array $a, array $b) {
        $size_a = count($a[DataStorage::KEY_MATCHES]);
        $size_b = count($b[DataStorage::KEY_MATCHES]);
        if ($size_a > $size_b) {
            return -1;
        } else if ($size_a < $size_b) {
            return 1;
        }
        return 0;
    }

    public function run() {
        $search_tags = explode(
            ',',
            $this->getArguments()[2]
        );
        $results = DataStorage::getInstance()->search($search_tags);
        usort(
            $results,
            [
                $this,
                'sortResults'
            ]
        );
        foreach ($results AS $filename => $attributes) {
            fprintf(
                STDOUT,
                " - \"%s\"\n\t - %s\n",
                $attributes[DataStorage::KEY_FILENAME],
                implode("\n\t - ", $attributes[DataStorage::KEY_MATCHES])
            );
        }
        fprintf(
            STDOUT,
            gettext("%d results") ."\n",
            count($results)
        );
    }

}
