<?php

namespace org\documentator;

abstract class CommandHandler {

	/* @var array $arguments */
	private $arguments;

    /* @var string $description */
    private $description;

    /* @var string $argument_description */
    private $argument_description;

    /**
     * Creates a new command handler.
     * @param array $arguments Arguments given by the user
     * @param string $description Description for this command
     * @param string $argument_description Description for the arguments
     * @param int $minimumofrequiredarguments Number of arguments required for this command
     */
	protected function __construct(array $arguments, string $description, string $argument_description, int $minimumofrequiredarguments = 0) {
		if ((count($arguments) - 2) < $minimumofrequiredarguments) {
			throw new \Exception(gettext("Not enough arguments given!"));
		}
		$this->arguments = $arguments;
        $this->description = $description;
        $this->argument_description = $argument_description;
        $this->minimumofrequiredarguments = $minimumofrequiredarguments;
	}

    public function getDescription(): string {
        return $this->description;
    }

    public function getArgumentDescription(): string {
        return $this->argument_description;
    }

	protected function getArguments(): array {
		return $this->arguments;
	}

	/**
	 * Executes the command.
	 */
	abstract public function run();

}
