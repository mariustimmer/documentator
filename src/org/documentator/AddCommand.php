<?php

namespace org\documentator;

use \org\documentator\CommandHandler AS CommandHandler;
use \org\documentator\DataStorage AS DataStorage;

class AddCommand extends CommandHandler {

    public function __construct(array $arguments) {
        parent::__construct(
            $arguments,
            gettext("Adds a new document to the data storage"),
            gettext("<filename> [tags]"),
            1
        );
    }

	private function tagsGiven(): bool {
		return (count($this->getArguments()) > 3);
	}

	public function run() {
        $filename = str_replace(__DIR__, '', realpath($this->getArguments()[2]));
		$tags = [];
		if ($this->tagsGiven()) {
			$tags = explode(',', $this->getArguments()[3]);
		}
        if (!file_exists($filename)) {
            fprintf(
                STDERR,
                "%s: %s\n",
                gettext("Document does not exist"),
                $filename
            );
            return;
        }
        if (DataStorage::getInstance()->containsDocument($filename)) {
			fprintf(
				STDOUT,
				"%s\n",
                gettext("Document will be updated")
			);
        }
		if (DataStorage::getInstance()->addDocument($filename, $tags)) {
			fprintf(
				STDOUT,
				"OK\n"
			);
		}
	}

}
